# Readme for fluxbox theme bgstack15a
License: CC-BY-SA 3.0 originally by tenner
Date: 2020-01-31
Modified from `slim_orange` from https://tenr.de/styles/?i=8
A few elements copied in from my first fluxbox theme bgstack15, including:
 * unselect.xpm -> unselected.xpm

2021-10-20 for bgstack15a-big, I resized the pixmaps.

    cd /usr/share/fluxbox/styles/bgstacka-big/pixmaps
    for word in $( grep -l -E '^"16 [0-9]+ [0-9]+ [0-9]+",' *xpm ) ; do new="$( echo "${word}" | sed -r -e 's/\.xpm//;' )24.xpm" ; convert "${word}" -resize 24x24 "${new}" ; done
    for word in *24.xpm ; do new="$( echo "${word}" | sed -r -e 's/24//g;' )" ; mv "${word}" "${new}" ; done

I also adjusted the `theme.cfg` manually.
