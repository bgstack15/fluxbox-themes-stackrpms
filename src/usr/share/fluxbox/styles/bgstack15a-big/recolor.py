#!/usr/bin/env python3
# Author: bgstack15
# Purpose: easily recolor slim_orange theme.
# Startdate: 2020-01-30
import os, sys, re, pathlib
from base64 import b16encode

# loop through all files
indir="/home/bgstack15/.fluxbox/styles/abcd/.pixmaps-orig"
outdir="/home/bgstack15/.fluxbox/styles/abcd/pixmaps"

# how much to multiple the values by, RGB
colorize = [ 130, 80, 90 ]

def hex_to_rgb(instring):
   # reference: https://stackoverflow.com/questions/29643352/converting-hex-to-rgb-value-in-python/29643643#29643643
   h = instring.lstrip('#')
   return tuple(int(h[i:i+2], 16) for i in (0, 2, 4))

def multiply_rgb(intuple,include_grays):
   if include_grays or not (intuple[0] == intuple[1] and intuple[1] == intuple[2]):
      a = max(min(int(intuple[0] * (colorize[0]/100)),255),0)
      b = max(min(int(intuple[1] * (colorize[1]/100)),255),0)
      c = max(min(int(intuple[2] * (colorize[2]/100)),255),0)
   else:
      a = intuple[0]
      b = intuple[1]
      c = intuple[2]
   newtuple = ( a, b, c)
   #newtuple = ( int(intuple[0] * (colorize[0]/100)), int(intuple[1] * (colorize[1]/100)), int(intuple[2] * (colorize[2]/100)))
   return newtuple

def rgb_to_hex(intuple):
   #return "#" + str(intuple[0]) + str(intuple[1]) + str(intuple[2])
   return str(b'#'+b16encode(bytes(intuple)))

def rgb2hex(color):
   # reference: https://stackoverflow.com/questions/3380726/converting-a-rgb-color-tuple-to-a-six-digit-code-in-python/49255135#49255135
   """Converts a list or tuple of color to an RGB string

   Args:
     color (list|tuple): the list or tuple of integers (e.g. (127, 127, 127))

   Returns:
     str:  the rgb string
   """
   return f"#{''.join(f'{hex(c)[2:].upper():0>2}' for c in color)}"

def recolor(filename,include_grays):
   newfilename = filename + "2"
   with open(indir+"/"+filename,'r') as f:
      lines = f.readlines()
   lines2=[]
   for line in lines:
      line2 = line.strip()
      a = re.search("\#[0-9A-Fa-f]{6}", line2)
      if a:
         # need to augment string a[0]
         b = hex_to_rgb(a[0])
         b = rgb2hex(multiply_rgb(b,include_grays))
         #print("%s becomes %s" % (str(a[0]),str(b)))
         line2=re.sub(a[0],str(b),line2,count=1)
      #print("%s" % line2)
      lines2 += {line2}
   with open(indir+"/"+newfilename,'w') as f:
      for line in lines2:
         f.write("%s\n" % line)
      # move new file over old one
      pathlib.Path(indir+"/"+newfilename).rename(outdir+"/"+filename)

for filename in os.listdir(indir):
#if True:
#    filename = "menuiconfcs.xpm"
   print(indir+"/"+filename)
   if re.match(".*\.xpm",filename):
      recolor(filename,False)
