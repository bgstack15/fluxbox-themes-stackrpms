# Readme for fluxbox theme bgstack15a
License: CC-BY-SA 3.0 originally by tenner
Date: 2020-01-31
Modified from `slim_orange` from https://tenr.de/styles/?i=8
A few elements copied in from my first fluxbox theme bgstack15, including:
 * unselect.xpm -> unselected.xpm
