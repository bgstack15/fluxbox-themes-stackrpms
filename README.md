# Readme for fluxbox-themes-stackrpms
The package stores my fluxbox styles.

## Upstream
[https://gitlab.com/bgstack15/fluxbox-themes-stackrpms](https://gitlab.com/bgstack15/fluxbox-themes-stackrpms)
This *is* the upstream!

## Reason for being in stackrpms
This is my own package.

## Alternatives
For xdg-compliant DEs, see [xdg-themes-stackrpms](https://gitlab.com/bgstack15/stackrpms/-/tree/master/xdg-themes-stackrpms).

## Reverse dependency matrix
Distro | fluxbox-themes-stackrpms version
------ | --------------------
all    | (master branch)

## Additional info

## Differences from upstream
None
