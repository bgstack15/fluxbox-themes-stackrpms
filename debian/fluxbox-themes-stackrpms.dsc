Format: 3.0 (quilt)
Source: fluxbox-themes-stackrpms
Binary: fluxbox-themes-stackrpms
Architecture: all
Version: 0.0.2-1
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://bgstack15.wordpress.com/
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 12~)
Package-List:
 fluxbox-themes-stackrpms deb x11 optional arch=all
Files:
 00000000000000000000000000000000 1 fluxbox-themes-stackrpms.orig.tar.gz
 00000000000000000000000000000000 1 fluxbox-themes-stackrpms.debian.tar.xz
